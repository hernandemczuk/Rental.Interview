using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace RentalService.Tests
{
    [TestClass]
    public class CustomerRentingTest
    {
        private readonly CustomerRenting _customerRenting;
        private readonly CustomerRenting _customerRentingWith0Rentals;
        private readonly CustomerRenting _customerRentingWith1Rentals;
        private readonly CustomerRenting _customerRentingWith2Rentals;
        private readonly CustomerRenting _customerRentingWith3Rentals;
        private readonly CustomerRenting _customerRentingWith4Rentals;
        private readonly CustomerRenting _customerRentingWith5Rentals;
        private readonly CustomerRenting _customerRentingWith6Rentals;
        private readonly Rental _hourlyRental;
        private readonly Rental _dailyRental;
        private readonly Rental _weeklyRental;

        public CustomerRentingTest()
        {
            var _mockFamiliarRental = new Mock<FamiliarRental>();

            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 0)).Returns(0);
            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 10)).Returns(0);
            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 20)).Returns(0);
            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 40)).Returns(12);
            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 60)).Returns(18);
            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 80)).Returns(24);
            _mockFamiliarRental.Setup(x => x.Calculate(It.IsAny<int>(), 100)).Returns(0);

            _customerRenting = new CustomerRenting(_mockFamiliarRental.Object);

            _customerRentingWith0Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 0,
                TotalRentals = 0
            };

            _customerRentingWith1Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 10,
                TotalRentals = 1
            };

            _customerRentingWith2Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 20,
                TotalRentals = 2
            };

            _customerRentingWith3Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 40,
                TotalRentals = 3
            };

            _customerRentingWith4Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 60,
                TotalRentals = 4
            };

            _customerRentingWith5Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 80,
                TotalRentals = 5
            };

            _customerRentingWith6Rentals = new CustomerRenting(_mockFamiliarRental.Object)
            {
                TotalCharged = 100,
                TotalRentals = 6
            };

            _hourlyRental = RentalFactory.Create(RentalEnum.Hourly);
            _dailyRental = RentalFactory.Create(RentalEnum.Daily);
            _weeklyRental = RentalFactory.Create(RentalEnum.Weekly);
        }

        [TestMethod]
        public void HourlyRental_For_1_Hour_Should_Charge_5()
        {
            _customerRenting.Rent(_hourlyRental, 1);
            Assert.AreEqual(5, _customerRenting.TotalCharged);
        }

        [TestMethod]
        public void HourlyRental_For_2_Hours_Should_Charge_10()
        {
            _customerRenting.Rent(_hourlyRental, 2);
            Assert.AreEqual(10, _customerRenting.TotalCharged);
        }

        [TestMethod]
        public void DailyRental_For_1_Day_Should_Charge_20()
        {
            _customerRenting.Rent(_dailyRental, 1);
            Assert.AreEqual(20, _customerRenting.TotalCharged);
        }

        [TestMethod]
        public void DailyRental_For_2_Days_Should_Charge_40()
        {
            _customerRenting.Rent(_dailyRental, 2);
            Assert.AreEqual(40, _customerRenting.TotalCharged);
        }

        [TestMethod]
        public void WeeklyRental_For_1_Week_Should_Charge_60()
        {
            _customerRenting.Rent(_weeklyRental, 1);
            Assert.AreEqual(60, _customerRenting.TotalCharged);
        }

        [TestMethod]
        public void WeeklyRental_For_2_Weeks_Should_Charge_120()
        {
            _customerRenting.Rent(_weeklyRental, 2);
            Assert.AreEqual(120, _customerRenting.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_0_Rentals_Should_Charge_0()
        {
            _customerRentingWith0Rentals.Checkout();
            Assert.AreEqual(0, _customerRentingWith0Rentals.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_1_Rentals_Should_Charge_10()
        {
            _customerRentingWith1Rentals.Checkout();
            Assert.AreEqual(10, _customerRentingWith1Rentals.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_2_Rentals_Should_Charge_20()
        {
            _customerRentingWith2Rentals.Checkout();
            Assert.AreEqual(20, _customerRentingWith2Rentals.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_3_Rentals_Should_Charge_28()
        {
            _customerRentingWith3Rentals.Checkout();
            Assert.AreEqual(28, _customerRentingWith3Rentals.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_4_Rentals_Should_Charge_42()
        {
            _customerRentingWith4Rentals.Checkout();
            Assert.AreEqual(42, _customerRentingWith4Rentals.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_5_Rentals_Should_Charge_56()
        {
            _customerRentingWith5Rentals.Checkout();
            Assert.AreEqual(56, _customerRentingWith5Rentals.TotalCharged);
        }

        [TestMethod]
        public void Checkout_For_6_Rentals_Should_Charge_100()
        {
            _customerRentingWith6Rentals.Checkout();
            Assert.AreEqual(100, _customerRentingWith6Rentals.TotalCharged);
        }
    }
}