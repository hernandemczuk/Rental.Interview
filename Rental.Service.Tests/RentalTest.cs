using Microsoft.VisualStudio.TestTools.UnitTesting;
using RentalService;

namespace RentalService.Tests
{
    [TestClass]
    public class RentalTest
    {
        public RentalTest(){}

        [TestMethod]
        public void RentalFactory_Hourly_Should_Return_New_HourlyRental()
        {
            var rental = RentalFactory.Create(RentalEnum.Hourly);
            Assert.AreEqual(new HourlyRental().GetType(), rental.GetType());
        }

        [TestMethod]
        public void RentalFactory_Daily_Should_Return_New_DailyRental()
        {
            var rental = RentalFactory.Create(RentalEnum.Daily);
            Assert.AreEqual(new DailyRental().GetType(), rental.GetType());
        }

        [TestMethod]
        public void RentalFactory_Weekly_Should_Return_New_WeeklyRental()
        {
            var rental = RentalFactory.Create(RentalEnum.Weekly);
            Assert.AreEqual(new WeeklyRental().GetType(), rental.GetType());
        }

        [TestMethod]
        public void RentalFactory_Default_Should_Return_Not_Implemented_Exception()
        {
            System.NotImplementedException notImplementedException = null;

            try
            {
                RentalFactory.Create((RentalEnum)4);
            }
            catch(System.NotImplementedException ex)
            {
                notImplementedException = ex;
            }

            Assert.IsNotNull(notImplementedException);
        }
    }
}