﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentalService.Tests
{
    [TestClass]
    public class FamiliarRentalTest
    {
        public FamiliarRentalTest()
        {
        }

        [TestMethod]
        public void Calculate_Outside_Min_Border_Should_Return_0()
        {
            var familiarRent = new FamiliarRental();

            var discount = familiarRent.Calculate(2, 20);
            Assert.AreEqual(0, discount);
        }

        [TestMethod]
        public void Calculate_Min_Border_Should_Return_Positive()
        {
            var familiarRent = new FamiliarRental();

            var discount = familiarRent.Calculate(3, 20);
            Assert.IsTrue(discount > 0);
        }

        [TestMethod]
        public void Calculate_Between_Borders_Should_Return_Positive()
        {
            var familiarRent = new FamiliarRental();

            var discount = familiarRent.Calculate(4, 20);
            Assert.IsTrue(discount > 0);
        }

        [TestMethod]
        public void Calculate_Max_Border_Should_Return_Positive()
        {
            var familiarRent = new FamiliarRental();

            var discount = familiarRent.Calculate(5, 20);
            Assert.IsTrue(discount > 0);
        }

        [TestMethod]
        public void Calculate_Outside_Max_Border_Should_Return_0()
        {
            var familiarRent = new FamiliarRental();

            var discount = familiarRent.Calculate(6, 20);
            Assert.AreEqual(0, discount);
        }
    }
}
