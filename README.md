# Rental.Interview

# Context
A company rents bikes under following options:

1. Rental by hour, charging $5 per hour
2. Rental by day, charging $20 a day
3. Rental by week, changing $60 a week
4. Family Rental, is a promotion that can include from 3 to 5 Rentals (of any type) with a discount of 30% of the total price

# Assigment:
1. Implement a set of classes to model this domain and logic
2. Add automated tests to ensure a coverage over 85%
3. Use GitHub to store and version your code
4. Apply all the recommended practices you would use in a real project
5. Add a README.md file to the root of your repository to explain: your design, the development practices you applied and how run the tests.

Note: we don't expect any kind of application, just a set of classes with its automated tests.

# Deliverables:
The link to your repository 

---------------------------------------------------------------------------------------------------------

# Domain

Our universe consists of two main classes; Rental and CustomerRenting (which is actually the person renting the bikes).

# Process

In my solution, I thought about implementing a process where the renting and the checkout are separeted actions. Because the specification said that there is a promotion that could be applied if the quantity of rentals were between 3 to 5, no discount must to be applied if the person continues renting after 5 rentals.

So if I've chosen to validate this promotion on each rental the customer makes, the result would be that after the rental number 5, the total ammount charged to this customer will consider a discount applied in the past (which is wrong).

# Rentals

My rental model consists of:

Rental (abstract class)
- HourlyRental
- DailyRental
- WeeklyRental

Separating the rental class means that each derived class can integrate its own logic in the future. Perhaps tomorrow, only hourly rentals can apply for familiar promotions or, even better, new kinds of rental (not only time related ones) can been added easily to the model.

Rental class makes sure that each derived class uses its GetPrice method (because its logic is the same between them, the implementation appears only in one place, so it complies with DRY), and of course, it provides a Rate property to instantiate with its proper value accordingly.

The process of instantiation of theses classes occurs also in just one place: RentalFactory. Here you can change your instatiation logic once and replicate the change in your whole system.  

The RentalEnum is just an enumeration that can be used by any consumer of this class in order to provide them with the right class.

# Familiar Rental

Should this be another derived class of Rental? Should this be a logic inside each derived class from Rental? This got me thinking a while, but then I realize that because the logic is about the quantity of rentals made by a costumer, it is more related to the costumer than the rentals itself.

This class only exposes the Calculate method, which will return the discount applied to the customer.

# Consumer Renting

This is the class that acts as front door for the whole project. It carries data about the renting process (TotalCharged, TotalRentals) and the two methods described before: Rent and Checkout. Rent has to be invoked on every renting the costumer makes, and Checkout only after the last one. 

Inside Checkout there is a FamiliarRental dependency, which is abstraced with and interface so we can mock it in our tests.

# Test Project

To run tests from console (needs dotnet core), hit dotnet test from Rental.Interview/Rental.Service.Tests or if you are in Visual Studio, just use the GUI for running the tests.