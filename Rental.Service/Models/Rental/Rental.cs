namespace RentalService
{
    public abstract class Rental
    {
        public double Rate { get; set; }

        public virtual double GetPrice(int timeQuantity) {
            return timeQuantity * this.Rate;
        }
    }
}