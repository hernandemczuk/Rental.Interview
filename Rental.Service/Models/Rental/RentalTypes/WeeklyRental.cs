namespace RentalService
{
    public class WeeklyRental : Rental
    {
        public WeeklyRental()
        {
            this.Rate = 60;
        }
    }
}