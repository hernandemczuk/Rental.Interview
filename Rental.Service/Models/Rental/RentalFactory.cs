namespace RentalService
{
    public static class RentalFactory
    {
        public static Rental Create(RentalEnum rental){
            switch(rental){
                case RentalEnum.Hourly: 
                    return new HourlyRental();
                case RentalEnum.Daily:
                    return new DailyRental();
                case RentalEnum.Weekly:
                    return new WeeklyRental();    
                default:
                    throw new System.NotImplementedException();
            }   
        }
    }
}