namespace RentalService
{
    public class FamiliarRental : IFamiliarRental {

        private double Discount { get; set; }
        private int MaxRentals { get; set; }
        private int MinRentals { get; set; }

        public FamiliarRental()
        {
            Discount = 0.3;
            MaxRentals = 5;
            MinRentals = 3;
        }

        private bool Validate(int totalRentals) {
            if(totalRentals >= this.MinRentals && totalRentals <= this.MaxRentals)
            {
                 return true;
            }
            return false;
        }

        private double GetDiscount(double totalCharged){
            return totalCharged * this.Discount;
        }

        public virtual double Calculate(int totalRentals, double totalCharged){
            return Validate(totalRentals) ? GetDiscount(totalCharged) : 0;
        }
    }
}