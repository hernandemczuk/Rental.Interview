﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalService
{
    public interface IFamiliarRental
    {
        double Calculate(int totalRentals, double totalCharged);
    }
}