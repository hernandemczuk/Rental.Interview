namespace RentalService
{
    public class CustomerRenting
    {
        private readonly IFamiliarRental _familiarRental;

        public CustomerRenting(IFamiliarRental familiarRental)
        {
            _familiarRental = familiarRental;
        }

        public Customer Customer { get; set; }

        public double TotalCharged { get; set; }

        public int TotalRentals { get; set; }

        public void Rent(Rental rental, int timeQuantity){
            this.TotalCharged += rental.GetPrice(timeQuantity);
            this.TotalRentals += 1;
        }

        public void Checkout(){
            var discount = _familiarRental.Calculate(this.TotalRentals, this.TotalCharged);
            this.TotalCharged -= discount;
        }
    }
}